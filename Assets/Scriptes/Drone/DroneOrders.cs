﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DroneOrders : MonoBehaviour
{
    [Header("Transforms")]
    public Transform camTransform;
    public Transform crossHair;

    [Header("UI")]
    public Canvas orderUI;
    public GameObject healUI;
    public GameObject rushUI;
    public GameObject returnUI;
    public GameObject shockUI;
    public GameObject iemUI;
    
    [Header("")]
    public GameObject target;

    [Header("Layer")]
    public LayerMask enemies;

    [Header("Scripts")]
    public DroneCapacities droneCapacities;
    public DroneStop droneStop;

    [Header("Bools")]
    public bool heal;
    public bool rush;
    public bool shock;
    public bool iem;

    [Header("CD")] 
    public float healCD;
    public float actualHealCD;
    public float rushCD;
    public float actualRushCD;
    public TextMeshProUGUI healCDText;
    public TextMeshProUGUI rushCDText;
    

    void Start()
    {
        //Initialisation des variables
        droneCapacities = GetComponent<DroneCapacities>();
        healUI.SetActive(false);
        rushUI.SetActive(false);
        returnUI.SetActive(false);
        shockUI.SetActive(false);
        iemUI.SetActive(false);
        actualHealCD = 0f;
        actualRushCD = 0f;
    }

    
    void Update()
    {
        //Direction du raycast
        var dir = camTransform.position - crossHair.position;
        
        if (GetComponent<DroneController>().isActive == false && Input.GetButton("Fire2"))
        {
            //Activation de l'UI
            orderUI.gameObject.SetActive(true);

            //Raycast pour cibler un ennemi
            RaycastHit hit;
            if (Physics.Raycast(camTransform.position, -dir, out hit, 150f, enemies))
            {
                target = hit.transform.gameObject;
            }
            Debug.DrawRay(camTransform.position, -dir * 150f, Color.red);

            //Gestion des 5 commandes du drone en fonction de la touche appuyé (1,2,3,4,5)
            if (Input.GetButtonDown("Drone1"))
            {
                if (heal)
                {
                    if (actualHealCD <= 0f)
                    {
                        droneCapacities.Heal();
                        actualHealCD = healCD;
                    }
                }
            }
            else if (Input.GetButtonDown("Drone2"))
            {
                if (rush)
                {
                    if (target.CompareTag("Enemy") || target.CompareTag("Robot"))
                    {
                        if (actualRushCD <= 0f)
                        {
                            droneCapacities.Rush();
                            actualRushCD = rushCD;
                        }
                    }
                }
            }
            else if (Input.GetButtonDown("Drone3"))
            {
                if (shock)
                {
                    droneCapacities.Shock();
                }
            }
            else if (Input.GetButtonDown("Drone4"))
            {
                if (iem)
                {
                    droneCapacities.IEM();
                }
            }
            else if (Input.GetButtonDown("Drone5"))
            {
                if (rush)
                {
                    droneCapacities.Return();
                }
            }
            
        }
        //Désaactvation de l'UI si on appuie pas sur fire2
         else
        {
            orderUI.gameObject.SetActive(false);
        }

        //Gestion des CD + affichage dans l'UI
        if (actualHealCD > 0f)
        {
            healCDText.gameObject.SetActive(true);
            actualHealCD -= Time.deltaTime;
            healCDText.text = actualHealCD.ToString("#.00");
        }
        else
        {
            healCDText.gameObject.SetActive(false);
        }

        if (actualRushCD > 0f)
        {
            rushCDText.gameObject.SetActive(true);
            actualRushCD -= Time.deltaTime;
            rushCDText.text = actualRushCD.ToString("#.00");
        }
        else
        {
            rushCDText.gameObject.SetActive(false);
            droneStop.rushed = false;
        }
    }

    //Gestion du débloquage des capacités
    public void ActiveHeal()
    {
        heal = true;
        healUI.SetActive(true);
    }
    
    public void ActiveRush()
    {
        rush = true;
        rushUI.SetActive(true);
        returnUI.SetActive(true);
    }
    
    public void ActiveShock()
    {
        shock = true;
        shockUI.SetActive(true);
    }
    
    public void ActiveIEM()
    {
        iem = true;
        iemUI.SetActive(true);
    }
}
