﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneCapacities : MonoBehaviour
{

   [Header("Scripts")]
   public DroneAOE droneAoe;
   public DroneMovement droneMovement;
   public PlayerHealth playerHealth;
   public DroneOrders droneOrders;

   [Header("stats")]
   public float heal;

   private void Start()
   {
      //Initialisation des variables
      droneAoe = GetComponent<DroneAOE>();
      droneMovement = GetComponent<DroneMovement>();
      playerHealth = GameObject.Find("Player").GetComponent<PlayerHealth>();
      droneOrders = GetComponent<DroneOrders>();
   }
   
   //Fonction du Heal
   public void Heal()
   {
      playerHealth.actualHp += heal;
   }

   //Foncion du Rush
   public void Rush()
   {
      droneMovement.actualmaxSpeed *= 5; //Augmentation du max speed du drone
      droneMovement.acceleration *= 5;  // Augmentation de l'acceleration du drone
      droneMovement.stay = false;         // 
      droneMovement.slow = false;         // désactivation des bool pour que le drone puisse se déplacer
      droneMovement.target = droneOrders.target; // assignation du target
      droneMovement.returned = false;   // désactivation du bool pour pouvair rappeler le drone
   }

   //fonction du Shock
   public void Shock()
   {
      droneAoe.Shock();
   }

   //fonction de l'IEM
   public void IEM()
   {
      droneAoe.IEM();
   }

   //Fonction du 
   public void Return()
   {
      if (droneMovement.returned == false)
      {
         droneMovement.Return(); 
      }
   }
   
}
