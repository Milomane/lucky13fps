﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneStop : MonoBehaviour
{
    [Header("Scripts")]
    public DroneMovement droneMovement;

    [Header("Bools")] 
    public bool rushed;

    private void Start()
    {
        //Initialisation des variables
        droneMovement = GetComponentInParent<DroneMovement>();
        rushed = false;

    }

    private void OnTriggerEnter(Collider other) //Quand un objet entre dans le collider
    {
        if (other.gameObject == droneMovement.target) //Comparaison de l'objet avec le target de DroneMovement
        {
            droneMovement.stay = true; // le drone s'arrête 
             if (other.gameObject == droneMovement.target && other.CompareTag("Enemy") || other.gameObject == droneMovement.target && other.CompareTag("Robot")) //si l'objet est un ennemi
             {
                 if (!rushed)
                 {
                     other.GetComponent<EnemyHealth>().TakeDamage(10); //inflige 5 dommages
                     rushed = true;
                 }
             }
        }
    }

    private void OnTriggerExit(Collider other)//Quand un objet sort du collider
    {
        if (other.gameObject == droneMovement.target) //Comparaison de l'objet avec le target de DroneMovement
        {
            droneMovement.stay = false; // le drone peux se déplacer
        }
    }
}
