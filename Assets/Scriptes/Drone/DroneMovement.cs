﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneMovement : MonoBehaviour
{
    [Header("GameObjects")]
    public GameObject target;
    public GameObject playerPos;

    [Header("Stats")]
    public float speed;
    public float maxSpeed;
    public float actualmaxSpeed;
    public float minSpeed;
    public float acceleration;
    public float returnAcceleration;
    private float _step;
    
    public float idleFrequency;
    public float idleMagnitude;

    [Header("Bools")]
    public bool stay;
    public bool slow;
    public bool returned;

    private Vector3 _pos;

    void Start()
    {
        //Initialisation des variables
        _pos = transform.position;
        _step = speed * Time.deltaTime;
        speed = 0;
        actualmaxSpeed = maxSpeed;
        Return();
    }


    void Update()
    {
        _pos = transform.position;
        _step = speed * Time.deltaTime;

        //Gestion si le drone se déplace ou pas
        if (stay)
        {
            Idle();
            speed = 0;
        }
        else
        {
            Moving();
        }
    }

    
    void Moving()
    {
        //Getion de la vitesse
        if (speed < actualmaxSpeed && !slow)
        {
            speed += acceleration;
        }
         else if (speed > minSpeed && slow)
         {
             speed -= acceleration;
         }
        else if (speed < minSpeed && slow)
        {
            speed += acceleration;
        }
        else if(speed >= actualmaxSpeed)
        {
            speed = actualmaxSpeed;
        }
        
        //Gestion du déplecement
        transform.position = Vector3.MoveTowards(_pos, target.transform.position, _step);
    }
    
    //Gestion de l'idle
    void Idle()
    { 
        transform.position = _pos + transform.up * Mathf.Sin(Time.time * idleFrequency) * idleMagnitude * Time.deltaTime;
    }

    //Gestion du player qui deviens la target
    public void Return()
    {
        target = playerPos;
        actualmaxSpeed = maxSpeed;
        acceleration = returnAcceleration;
        slow = false;
        stay = false;
        returned = true;
    }
}
