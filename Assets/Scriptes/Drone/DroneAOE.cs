﻿﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class DroneAOE : MonoBehaviour
{
    [Header("GameObjects")]
    public GameObject shockVFX;
    public GameObject iemVFX;

    [Header("Stats")]
    public float aoeRadius;
    public float actualRadius;
    public float increase;
    public float duration;
    public float stunDuration;
   

    [Header("Layers")]
    public LayerMask shockTargets;
    public LayerMask iemTargets;

    [Header("Bools")]
    public bool isActive;
    public bool shockActive;
    public bool iemActive;
    
    [Header("")]
    public RaycastHit[] hits;
    public Slider cdSlider;

    [Header("CD")]
    public float aoeCD;
    public float actualCD;
    public float cdForText;
    public TextMeshProUGUI cdTextShock;
    public TextMeshProUGUI cdTextIem;
    
    void Start()
    {
        //Initialisation des variables
        actualRadius = 0;
        actualCD = aoeCD;
        shockVFX.SetActive(false);
        iemVFX.SetActive(false);
        cdSlider.maxValue = aoeCD;
    }

    
    void Update()
    {
        //Gestion de l'AOE
        if (isActive)
        {
            //Augmentation du radius
            actualRadius += increase;
            if (actualRadius >= aoeRadius)
            {
                actualRadius = aoeRadius;
            }
            
            //Gestion du Shock
            if (shockActive)
            {
                Shock();
                shockVFX.SetActive(true);
                shockVFX.transform.localScale = new Vector3(actualRadius,actualRadius,actualRadius)*2;
            }
            //Gestion de l'IEM
            else if(iemActive)
            {
                IEM();
                iemVFX.SetActive(true);
                iemVFX.transform.localScale = new Vector3(actualRadius,actualRadius,actualRadius)*2;
            }
        }
        //Réinitialisation du radius et VFX
        else
        {
            actualRadius = 0;
            shockVFX.transform.localScale = Vector3.one;
            shockVFX.SetActive(false);
            iemVFX.transform.localScale = Vector3.one;
            iemVFX.SetActive(false);
        }

        //Gestion du CD
        if (actualCD < aoeCD)
        {
            actualCD += Time.deltaTime;
        }

        //Gestion de l'affichage dans l'UI
        if (cdForText > 0)
        {
            cdTextShock.gameObject.SetActive(true);
            cdTextIem.gameObject.SetActive(true);
            cdForText -= Time.deltaTime;
            cdTextIem.text = cdForText.ToString("#.00");
            cdTextShock.text = cdForText.ToString("#.00");
        }
        else
        {
            cdTextShock.gameObject.SetActive(false);
            cdTextIem.gameObject.SetActive(false);
        }
        
        //Gestion du CDslider dans la phase playable
        cdSlider.value = actualCD;
        
    }

    //Gestion de la durée de l'AOE
    IEnumerator AOE()
    {
        yield return new WaitForSeconds(duration);
        isActive = false;
        shockActive = false;
        iemActive = false;
    }

    //Fonction du shock
    public void Shock()
    {
        if (actualCD >= aoeCD)
        {
            isActive = true;
            shockActive = true;
            actualCD = 0;
            cdForText = aoeCD;
            StartCoroutine(AOE());
        
            hits = Physics.SphereCastAll(transform.position, actualRadius, Vector3.forward, 0f, shockTargets);
            foreach (var enemy in hits)
            {
                enemy.transform.gameObject.GetComponent<EnemyStun>().Stun(stunDuration,0f);
            }
        }
    }

    //Fonction de L'IEM
    public void IEM()
    {
        if (actualCD >= aoeCD)
        {
            isActive = true;
            iemActive = true;
            actualCD = 0;
            cdForText = aoeCD;
            StartCoroutine(AOE());
        
            hits = Physics.SphereCastAll(transform.position, actualRadius, Vector3.forward, 0f, iemTargets);
            foreach (var enemy in hits)
            {
                enemy.transform.gameObject.GetComponent<EnemyStun>().Stun(stunDuration,0f);
            }
        }
    }
    
    //Gestion du Guizmo
    void OnDrawGizmos()
    {
        if (isActive)
        {
            if (shockActive)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawWireSphere(transform.position, actualRadius);
            }
            else if(iemActive)
            {
                Gizmos.color = Color.white;
                Gizmos.DrawWireSphere(transform.position, actualRadius);
            }
            
        }
    }
}
