﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneController : MonoBehaviour
{
    [Header("GameObjects")]
    public Camera playerCamera;
    public Camera droneCamera;
    public GameObject[] colliders = new GameObject[2];
    public GameObject uiCanvas;
    
    [Header("Scripts")]
    private DroneMovement _droneMovement;

    [Header("stats")]
    public float speed;
    public float maxSpeed;
    public float sensivity;

    [Header("Bools")]
    public bool isActive;

    [Header("")]
    public Rigidbody rb;

    void Start()
    {
        //Initialisation des variables
        droneCamera.gameObject.SetActive(false);
        uiCanvas.SetActive(false);
        _droneMovement = gameObject.GetComponent<DroneMovement>();
        rb = GetComponent<Rigidbody>();
    }
    
    void Update()
    {
        
        if (isActive)
        {
            //Set les variables pour le bon fonctionnement de la phase playable
            _droneMovement.enabled = false;
            playerCamera.gameObject.SetActive(false);
            droneCamera.gameObject.SetActive(true);
            colliders[0].SetActive(false);
            colliders[1].SetActive(false);
            uiCanvas.SetActive(true);
            rb.constraints = RigidbodyConstraints.FreezeRotation;

            //Gestion du déplacement avec le Rigidbody
            rb.AddForce(transform.forward * Input.GetAxis("Vertical") * Time.deltaTime * speed); 
            rb.AddForce(transform.right * Input.GetAxis("Horizontal") * Time.deltaTime * speed); 
            rb.AddForce(transform.up * Input.GetAxis("Jump") * Time.deltaTime * speed/2); 
            rb.AddForce(transform.up * Input.GetAxis("Crouch") * Time.deltaTime * -speed/2);
            transform.Rotate(Vector3.up, Input.GetAxis("Mouse X") * Time.deltaTime * sensivity);
            
            //Gestion du max speed
            if (rb.velocity.magnitude > maxSpeed)
            { 
                rb.velocity = rb.velocity.normalized * maxSpeed;
            }
            
            //Gestion du ralentissement
            if (rb.velocity.magnitude > 0)
            { 
                rb.velocity = rb.velocity * 0.90f;
            }
            
            //Gestion du Rush (playable)
            if (Input.GetButtonDown("Fire1"))
            {
                maxSpeed = maxSpeed * 2;
                speed = speed * 4;
            }
            else if(Input.GetButtonUp("Fire1"))
            {
                maxSpeed = maxSpeed / 2;
                speed = speed / 4;
            }

            //Gestion de l'IEM (playable)
            if (Input.GetButtonDown("Fire2"))
            {
                GetComponent<DroneCapacities>().IEM();
            }
        }
        else
        {
            //Re set des variables pour désactiver la phase playable
            playerCamera.gameObject.SetActive(true);
            droneCamera.gameObject.SetActive(false);
            colliders[0].SetActive(true);
            colliders[1].SetActive(true);
            _droneMovement.enabled = true;
            uiCanvas.SetActive(false);
            rb.constraints = RigidbodyConstraints.FreezeAll;
        }
    }
}
