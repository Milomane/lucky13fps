﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneDeceleration : MonoBehaviour
{
    [Header("Scripts")]
    public DroneMovement droneMovement;
    
    private void Start()
    {
        //Initialisation des variables
        droneMovement = GetComponentInParent<DroneMovement>();
    }
    
    private void OnTriggerEnter(Collider other) //Quand un objet entre dans le collider
    {
        if (other.gameObject == droneMovement.target) // Comparaison de l'objet avec le target de DroneMovement
        {
            droneMovement.slow = true; //Le drone rallentit
        }
    }

    private void OnTriggerExit(Collider other) //Quand un objet sort du collider
    {
        if (other.gameObject == droneMovement.target) // Comparaison de l'objet avec le target de DroneMovement
        {
            droneMovement.slow = false; //Le drone peux accélérer
        }
    }
}
