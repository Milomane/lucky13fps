﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLastPos : MonoBehaviour
{
    public Vector3 lastPos;

    public List<Vector3> posList;

    public float maxArrayLenght;

    private void Start()
    {
        posList[0] = transform.position;
        lastPos = posList[0];
    }

    void Update()
    {
        lastPos = posList[0];
        posList.Add(transform.position);

        if (posList.Count > maxArrayLenght)
        {
            posList.RemoveAt(0);
        }
    }
}
