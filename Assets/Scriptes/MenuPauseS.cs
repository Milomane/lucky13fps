﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPauseS : MonoBehaviour
{
    public GameObject panelCompétence;
    public bool isVisible = false;
    public bool isInvisible;
    public string sceneaReload;
    public GameObject menu;
    public GameObject player;
    public int timeValue;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel") && !panelCompétence.activeSelf)
        {
            isVisible = true;
           
        }

        if (Input.GetButtonDown("Cancel") && isInvisible)
        {
            isVisible = false;
            isInvisible = false;
        }

        if (isVisible && !isInvisible)
        {
            timeValue = 0;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            menu.SetActive(true);
            isInvisible = true;
            player.GetComponent<PlayerController>().enabled = false ;
            player.GetComponent<Player_Shoot>().enabled = false;
        }

        if (isVisible == false && !panelCompétence.activeSelf)
        {
            menu.SetActive(false);
            timeValue = 1;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            player.SetActive(true);
            player.GetComponent<PlayerController>().enabled = true ;
            player.GetComponent<Player_Shoot>().enabled = true;
        }
    }

    public void Reload()
    {
       
        SceneManager.LoadScene(sceneaReload);
    }

    public void Quit()
    {
        Application.Quit();
    }
    
}
