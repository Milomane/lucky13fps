﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEquipementS : MonoBehaviour
{
    public GameObject player;
    public bool[] isEquiped = new bool[9];

    public int damageCAC;
    public int damage;
    public int damageReduction;

    public int tirRate;
    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        isEquiped =  player.GetComponent<menuEquipementS>().isButtonSelected;
        if (isEquiped[0])
        {
            damage = 1;
            tirRate = 1;
        }

        if (isEquiped[1])
        {
            damage = 2;
            tirRate = 1;
        }

        if (isEquiped[2])
        {
            damage = 2;
            tirRate = 3;
        }

        if (isEquiped[3])
        {
            damageCAC = 2;
        }

        if (isEquiped[4])
        {
            damageCAC = 3;
        }

        if (isEquiped[5])
        {
            damageCAC = 4;
        }

        if (isEquiped[6])
        {
            damageReduction = 0;
        }

        if (isEquiped[7])
        {
            damageReduction = 1;
        }

        if (isEquiped[8])
        {
            damageReduction = 2;
        }
    }
}
