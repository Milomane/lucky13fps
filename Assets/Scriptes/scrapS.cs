﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrapS : MonoBehaviour
{
    private Rigidbody rb;

    public int _randomForceX;

    public int _randomForceY;

    public int _randomForceZ;

    public int randomChoice1;

    public int randomChoice2;

    public LayerMask player;

    public float radiusSphere;
    // Start is called before the first frame update
    void Start()
    {
        _randomForceX = Random.Range(randomChoice1,randomChoice2);
        _randomForceY = Random.Range(randomChoice1, randomChoice2);
        _randomForceZ = Random.Range(randomChoice1, randomChoice2);
        rb = GetComponent<Rigidbody>();
        rb.AddForce(new Vector3(_randomForceX,_randomForceY,_randomForceZ));
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit[] rangePlayer = Physics.SphereCastAll(transform.position, radiusSphere, transform.TransformDirection(new Vector3(1,0,0)), 0, player);
        
        foreach (RaycastHit hit in rangePlayer)
        {
            Debug.Log("ça marche");
            Vector3 target = hit.transform.position;
             transform.position = Vector3.MoveTowards(transform.position, target,2 * Time.deltaTime);
        }
        
    }
    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, 3);
    }
}
