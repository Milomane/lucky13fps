﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraTrough : MonoBehaviour
{
  public Transform obstruction;
  public Material[] material;
 
  public Renderer rend;
 
  public Transform wallObstruction;
  public float zoomspeed = 2f;
  public void Start()
  {
    rend.enabled = true;
    rend.sharedMaterial = material[0];
  }

  public void Update()
  {
    Debug.DrawLine(transform.position, obstruction.position, Color.blue);
    
  }

  public void LateUpdate()
  {
    viewObstructed();
   
  }

  public void viewObstructed()
  {
    RaycastHit hit;
   
    if (Physics.Linecast(transform.position, obstruction.position , out  hit));
    {// Permet de voir a travers les murs et de rapprocher la caméra quand je joueur ce rapproche des murs

      if (hit.collider.gameObject.CompareTag("wall") || hit.collider.gameObject.CompareTag("ground"))
      { 
        Debug.Log("tessssssssssssssssst");
        wallObstruction = hit.transform;
        wallObstruction.gameObject.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;

        if (Vector3.Distance(wallObstruction.position, transform.position ) >= 2f && Vector3.Distance(transform.position, obstruction.position) >= 1.5f)
        {
          transform.Translate(Vector3.forward * (zoomspeed * Time.deltaTime));
        }
        
      } 
      else
      {
        wallObstruction.gameObject.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        
        if (Vector3.Distance(transform.position, obstruction.position)< 3f)
        {
          transform.Translate(Vector3.back * (zoomspeed * Time.deltaTime));
        }
      }
      if (hit.collider.gameObject.CompareTag("Drone"))
      {// Permet de rendre le drone transparent
        
        Debug.Log("hello");
        hit.collider.gameObject.transform.parent.GetComponent<MeshRenderer>().material = material[1];
      }
      else
      {
        hit.collider.gameObject.transform.parent.GetComponent<MeshRenderer>().material = material[0];
      }
      
    }

  }
}