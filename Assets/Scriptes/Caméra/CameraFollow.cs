﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public float cameraMoveSpeed = 120.0f;

    public GameObject cameraFollowObj;

    public Vector3 followPos;

    public float clampAngle = 80.0f;
    public float inputSensitivity = 150.0f;
    public GameObject cameraObj;

    public GameObject playerObj;

    public float camDistanceXToPlayer;
    public float camDistanceYToPlayer;
    public float camDistanceZToPlayer;

    public float mouseX;
    public float mouseY;

    public float finalInputX;
    public float finalInputZ;

    public float smoothX;
    public float smoothY;
    private float rotx = 0.0f;
    private float roty = 0.0f;
    // Start is called before the first frame update
    void Start()
    {    // Set up localRotation des sticks
        Vector3 rot = transform.localRotation.eulerAngles;
        roty = rot.y;
        roty = rot.x;
       Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        //Set up movement of the sticks
        float inputX = Input.GetAxis("RightStickHorizontal");
        float inputZ = Input.GetAxis("RightStickVertical");
        mouseX = Input.GetAxis("Mouse X");
        mouseY = Input.GetAxis("Mouse Y");
        finalInputX = inputX + mouseX;
        finalInputZ = inputZ + mouseY;


        roty += finalInputX * inputSensitivity * Time.deltaTime;
        rotx += finalInputZ * inputSensitivity * Time.deltaTime;

        rotx = Mathf.Clamp(rotx, -clampAngle, clampAngle);
        
        Quaternion localRotation = Quaternion.Euler(rotx, roty, 0.0f);
        transform.rotation = localRotation;
        
    }

    private void LateUpdate()
    {
        CameraUpdater();
    }

    public void CameraUpdater()
    {    //sET la target object a suivre
        Transform target = cameraFollowObj.transform;

        // Bouge  l'object vers la target
        float step = cameraMoveSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
    }
}
