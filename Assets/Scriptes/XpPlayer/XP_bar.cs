﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using  UnityEngine.UI;
public class XP_bar : MonoBehaviour
{
    
    public Slider xpSlider;
    public int beginxp = 0;
    public int Maxxp = 10;
    public int minxp = 0;


    public TextMeshProUGUI lvltext;
    
    public void BaseKarma(int karma)
    {// Karma de base du joueur
        xpSlider.minValue = minxp;
        xpSlider.maxValue = Maxxp;
        xpSlider.value = beginxp;
        Player_xp.startxp = 0;
    }

    public void SetKarma(int karma)
    {// Update le karma du joueur 

        xpSlider.value = Player_xp.startxp;
    }
    

    public void Update()
    {// Set up le text par rapport au niv du player
        if (Player_xp.levelxp > 0)
        {
            lvltext.text = Player_xp.levelxp.ToString();
        }
    }
}
