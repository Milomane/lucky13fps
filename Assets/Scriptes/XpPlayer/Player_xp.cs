﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_xp : MonoBehaviour
{
    public XP_bar xpBar;
    public static int startxp = 0;

    public static int levelxp = 0;
    public bool XpMax = true;

    // Start is called before the first frame update
    void Start()
    {
        levelxp = 0;
        startxp = 0;
    }

    // Update is called once per frame
    void Update()
    {

       /* if (Input.GetKeyDown(KeyCode.A) && XpMax)
        {
            // Augmente l'xp du player
            startxp++;
            xpBar.SetKarma(startxp);

           
        }*/




    }

    public void OnCollisionEnter(Collision other)
    {// Augmente l'xp du player
        if (other.gameObject.CompareTag("scrap") && XpMax)
        {
            Destroy(other.gameObject);
            startxp++;
            xpBar.SetKarma(startxp);
            if (xpBar.xpSlider.value >= xpBar.Maxxp)
            {
                xpBar.BaseKarma(startxp);
                levelxp++;

            }
        }
      

        if (levelxp == 4)
        {
            XpMax = false;
            xpBar.xpSlider.value = xpBar.xpSlider.maxValue;
        }
    }
}