﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrapRecuperationS : MonoBehaviour
{
    public int scrapValue;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("scrap"))
        {
            Destroy(other.gameObject);
            scrapValue++;
        }
    }
}
