﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panssement : MonoBehaviour
{
    private GameObject player;
    
    void Start()
    {
        player = FindObjectOfType<PlayerController>().gameObject;
    }
    void Update()
    {
        player.SetActive(true);
    }
}
