﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FolowPath : MonoBehaviour
{
    public Transform target;
    public bool follow = true;

    public float speed;

    private NavMeshAgent navAgent;

    public void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (follow)
        {
            navAgent.speed = speed;
            navAgent.destination = target.position;

            // Direction toward target

            /*
            transform.LookAt(new Vector3(target.position.x, transform.position.y, target.position.z), transform.up);
            
            //Debug.DrawRay(transform.position, Vector3.forward, Color.red);
            transform.Translate(Vector3.forward * speed* Time.deltaTime);
            */
        }
    }
}
