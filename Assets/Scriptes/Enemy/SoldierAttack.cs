﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class SoldierAttack : MonoBehaviour
{
    public float rangeRadius = 30;
    public float maxNearRange = 10;
    public LayerMask playerLayer;

    private GameObject player;
    private FolowPath followPath;
    private NavMeshAgent navAgent;
    private bool detectPlayer;

    public int numberOfRay = 10;
    public float coneAngle = 15;
    public float coneRange = 20;
    public float speedAfterDetect = 4;

    public GameObject bulletPrefab;
    public Transform bulletSpawnPos;
    public float shootRate = 5;
    public float inaccuracy = 1;
    public int chargeMax;
    private int charge;
    public float reloadTime;
    public bool reloading;
    private float timer;
    
    void Start()
    {
        charge = chargeMax;
        player = FindObjectOfType<PlayerController>().gameObject;
        followPath = GetComponent<FolowPath>();
        navAgent = GetComponent<NavMeshAgent>();
    }
    
    
    void Update()
    {
        if (GetComponent<EnemyHealth>().health <= 0)
        {
            return;
        }
        
        // Detect if is in range to agro
        bool inRange = false;
        
        RaycastHit[] sphereHits;
        sphereHits = Physics.SphereCastAll(transform.position, rangeRadius, Vector3.forward, rangeRadius, playerLayer);

        foreach (var sphereHit in sphereHits)
        {
            if (sphereHit.collider.GetComponent<PlayerController>())
            {
                inRange = true;
            }
        }
        
        // Detect the player in a cone
        if (inRange)
        {
            for (int i = 0; i < numberOfRay; i++)
            {
                RaycastHit hit;
                Physics.Raycast(transform.position, (transform.forward * coneRange) + ( (coneAngle / (numberOfRay-1)) * transform.right * i) - (coneAngle * transform.right) / 2, out hit, rangeRadius, playerLayer);
                Debug.DrawRay(transform.position, (transform.forward * coneRange) + ( (coneAngle / (numberOfRay-1)) * transform.right * i) - (coneAngle * transform.right) / 2, Color.red);
                
                
                if (hit.collider != null)
                {
                    if (hit.collider.gameObject == player)
                    {
                        detectPlayer = true;
                    }
                }
            }
        }
        else
        {
            detectPlayer = false;
        }

        
        
        // Aproach a player and shoot
        timer -= Time.deltaTime;
        if (detectPlayer)
        {
            // Aproach
            if (Vector3.Distance(transform.position, player.transform.position) > maxNearRange)
            {
                followPath.follow = false;
                navAgent.speed = speedAfterDetect;
                navAgent.destination = player.transform.position;
            }
            else
            {
                transform.LookAt(new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z));
                navAgent.destination = transform.position;
            }
            
            
            // Attack
            if (charge > 0 )
            {
                if (timer <= 0)
                {
                    timer = shootRate;
                    Vector3 inaccuracyVector = new Vector3(Random.Range(inaccuracy, -inaccuracy), Random.Range(inaccuracy, -inaccuracy), Random.Range(inaccuracy, -inaccuracy));
                
                    GameObject bullet = Instantiate(bulletPrefab, bulletSpawnPos.position, transform.rotation);
                    bullet.GetComponent<EnnemyBullet>().target = player.transform;
                    bullet.GetComponent<EnnemyBullet>().inaccuracyVector = inaccuracyVector;
                    charge--;
                }
            }
            else
            {
                if (!reloading)
                {
                    reloading = true;
                    StartCoroutine(Reload());
                }
            }
            
        }
        else
        {
            followPath.follow = true;
        }
    }

    public IEnumerator Reload()
    {
        yield return new WaitForSeconds(reloadTime);
        charge = chargeMax;
        reloading = false;
    }
    public void TakeDamageFromPlayer()
    {
        detectPlayer = true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, rangeRadius);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, maxNearRange);
    }
}
