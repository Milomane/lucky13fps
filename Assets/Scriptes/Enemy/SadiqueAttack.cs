﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class SadiqueAttack : MonoBehaviour
{
    public float rangeRadius = 30;
    public float maxNearRange = 2;
    public float attackRange = 2.5f;
    public LayerMask playerLayer;

    private GameObject player;
    private FolowPath followPath;
    private NavMeshAgent navAgent;
    private Animator animator;
    private bool detectPlayer;

    public int numberOfRay = 8;
    public float coneAngle = 15;
    public float coneRange = 20;
    public float speedAfterDetect = 6;
    
    public float attackRate = 3;
    public GameObject weapon;
    private float timer;
    
    void Start()
    {
        player = FindObjectOfType<PlayerController>().gameObject;
        followPath = GetComponent<FolowPath>();
        navAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
    }
    
    
    void Update()
    {
        if (GetComponent<EnemyHealth>().health <= 0)
        {
            return;
        }
        
        // Detect if is in range to agro
        bool inRange = false;
        
        RaycastHit[] sphereHits;
        sphereHits = Physics.SphereCastAll(transform.position, rangeRadius, Vector3.forward, rangeRadius, playerLayer);

        foreach (var sphereHit in sphereHits)
        {
            if (sphereHit.collider.GetComponent<PlayerController>())
            {
                inRange = true;
            }
        }
        
        // Detect the player in a cone
        if (inRange)
        {
            for (int i = 0; i < numberOfRay; i++)
            {
                RaycastHit hit;
                Physics.Raycast(transform.position, (transform.forward * coneRange) + ( (coneAngle / (numberOfRay-1)) * transform.right * i) - (coneAngle * transform.right) / 2, out hit, rangeRadius, playerLayer);
                Debug.DrawRay(transform.position, (transform.forward * coneRange) + ( (coneAngle / (numberOfRay-1)) * transform.right * i) - (coneAngle * transform.right) / 2, Color.red);
                
                
                if (hit.collider != null)
                {
                    if (hit.collider.gameObject == player)
                    {
                        detectPlayer = true;
                    }
                }
            }
        }
        else
        {
            detectPlayer = false;
        }

        
        
        // Aproach a player and shoot
        timer -= Time.deltaTime;
        if (detectPlayer)
        {
            // Aproach
            if (Vector3.Distance(transform.position, player.transform.position) > maxNearRange)
            {
                followPath.follow = false;
                navAgent.speed = speedAfterDetect;
                navAgent.destination = player.transform.position;
            }
            else
            {
                transform.LookAt(new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z));
                navAgent.destination = transform.position;
            }
            
            // Attack
            if (Vector3.Distance(transform.position, player.transform.position) <= attackRange)
            {
                if (timer <= 0)
                {
                    timer = attackRate;
                    StartCoroutine(Attack());
                    
                }
            }
        }
        else
        {
            followPath.follow = true;
        }
    }

    IEnumerator Attack()
    {
        animator.SetTrigger("Attack");
        weapon.GetComponent<EnemyMeleeWeapon>().active = true;
        yield return new WaitForSeconds(0.1f);
        yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length - 0.1f);
        weapon.GetComponent<EnemyMeleeWeapon>().active = false;
    }

    public void TakeDamageFromPlayer()
    {
        detectPlayer = true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, rangeRadius);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, maxNearRange);
    }
}
