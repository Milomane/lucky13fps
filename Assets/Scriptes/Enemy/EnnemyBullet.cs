﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemyBullet : MonoBehaviour
{
	public int damage;
    
	public float speed = 500;
	public float timeBeforeDestroy = 5;

	public Vector3 inaccuracyVector;
	public Transform target;

	public void Start()
	{
		transform.LookAt(target, Vector3.up);
		GetComponent<Rigidbody>().AddForce((transform.forward + inaccuracyVector) * speed);
        
		Destroy(gameObject, timeBeforeDestroy);
	}

	public void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<PlayerHealth>())
		{
			other.GetComponent<PlayerHealth>().TakeDamage(damage);
			Destroy(gameObject);
		}
	}

	public void OnCollisionEnter(Collision other)
	{
		if (other.collider.tag == "ground" || other.collider.tag == "wall")
		{
			Destroy(gameObject);
		}
	}
}