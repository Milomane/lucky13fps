﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStun : MonoBehaviour
{
    private float timer;
    
    public void Stun(float time, float speedMultiplier)
    {
        StartCoroutine(FollowStunTime(time, speedMultiplier));
        StartCoroutine(StunTime(time, speedMultiplier));
    }

    public void Update()
    {
        
    }

    IEnumerator StunTime(float time, float speedMultipier)
    {
        if (GetComponent<SadiqueAttack>())
        {
            SadiqueAttack attackScript = GetComponent<SadiqueAttack>();
            attackScript.TakeDamageFromPlayer();
            float saveSpeed = attackScript.speedAfterDetect;
            attackScript.speedAfterDetect = saveSpeed * speedMultipier;
            
            yield return new WaitForSeconds(time);

            attackScript.speedAfterDetect = saveSpeed;
        }
        else if (GetComponent<SoldierAttack>())
        {
            SoldierAttack attackScript = GetComponent<SoldierAttack>();
            attackScript.TakeDamageFromPlayer();
            float saveSpeed = attackScript.speedAfterDetect;
            attackScript.speedAfterDetect = saveSpeed * speedMultipier;
            
            yield return new WaitForSeconds(time);

            attackScript.speedAfterDetect = saveSpeed;
        }
    }

    IEnumerator FollowStunTime(float time, float speedMultipier)
    {
        if (GetComponent<FolowPath>())
        {
            FolowPath followScript = GetComponent<FolowPath>();
            float saveSpeed = followScript.speed;
            followScript.speed = saveSpeed * speedMultipier;
            
            yield return new WaitForSeconds(time);

            followScript.speed = saveSpeed;
        }
    }
}