﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMeleeWeapon : MonoBehaviour
{
    public int damage = 1;
    public float cooldown = 0.5f;
    private float timer;
    public bool active;
    
    void Update()
    {
        timer -= Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (active && timer <= 0)
        {
            if (other.GetComponent<PlayerHealth>())
            {
                timer = cooldown;
                other.GetComponent<PlayerHealth>().TakeDamage(damage);
            }
        }
    }
}
