﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path : MonoBehaviour
{
    public Transform nextPath;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<FolowPath>())
        {
            FolowPath otherPathCompoment = other.GetComponent<FolowPath>();

            if (otherPathCompoment.target != nextPath)
            {
                otherPathCompoment.target = nextPath;
            }
        }
    }
}
