﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int health;

    private Animator animator;
    private ParticleSystem bloodParticle;

    void Start()
    {
        animator = GetComponent<Animator>();
        bloodParticle = GetComponentInChildren<ParticleSystem>();
    }
    
    
    void Update()
    {
        if (health <= 0)
        {
            // Loot
            animator.SetTrigger("Death");
            Destroy(gameObject, animator.GetCurrentAnimatorStateInfo(0).length); 
        }

      //  if (Input.GetKeyDown(KeyCode.L))
       // {
        //    TakeDamage(1);
        //}
    }

    public void TakeDamage(int damage)
    {
        
        if (GetComponent<SoldierAttack>())
        {
            GetComponent<SoldierAttack>().TakeDamageFromPlayer();
        }
        
        bloodParticle.Play();
        health -= damage;
    }
}
