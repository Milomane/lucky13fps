﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeScaler : MonoBehaviour
{
    public float timeScale ;
    public GameObject menuObject;
    
    void Update()
    {
        timeScale = menuObject.GetComponent<MenuPauseS>().timeValue;
        Time.timeScale = timeScale;
    }
}
