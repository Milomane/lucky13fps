﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;
using Cursor = UnityEngine.Cursor;

public class activeUiCompétence: MonoBehaviour
{
    public GameObject uiPanelCompétence;
    public static bool uiactive;
    public PlayerController playerController;
    public CameraFollow cameraFollow;
    public Player_Shoot playerShoot;

    public List<Button> uibuttonCompétence;
    public TimeScaler time;
    
    // Start is called before the first frame update
    void Start()
    {
        uiactive = false;
    }

    // Update is called once per frame
    void Update()
    {// permet d'activer le panel de l'arbre de compétence
        if (Input.GetKeyDown(KeyCode.C) && uiactive == false && uiPanelCompétence.activeSelf == false)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            uiPanelCompétence.SetActive(true);
            time.timeScale = 0f;
            playerController.enabled = !playerController.enabled;
            cameraFollow.enabled = !cameraFollow.enabled;
            playerShoot.enabled = !playerShoot.enabled;
            uiactive = true;
         
            
        }
        foreach (Button button in uibuttonCompétence)
        {// Pour chaque bouton dans notre list on va d'abord les rend non intéractble et ensuite on check si on a atteint le niv d'xp requis cela débloque la compétence
            button.interactable = false;
            if (Player_xp.levelxp >= 1)
            {
                uibuttonCompétence[0].interactable = true;
            }

            if (Player_xp.levelxp >= 2)
            {
                uibuttonCompétence[1].interactable = true;
            }

            if (Player_xp.levelxp >= 3 )
            {
               
                uibuttonCompétence[2].interactable = true;
            }

            if (Player_xp.levelxp >= 4 )
            {
                uibuttonCompétence[3].interactable = true;
            }

        }

        
       
    
        
    }


    public void ExitUiPanel()
    {// permet de désactiver le panel de l'arbre de compétence
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        uiactive = false;
        time.timeScale = 1f;
        uiPanelCompétence.SetActive(false);
        playerController.enabled = !playerController.enabled;
        cameraFollow.enabled = !cameraFollow.enabled;
        playerShoot.enabled = !playerShoot.enabled;
    }



    
    public void BonusLevel1()
    {//Active le BonusLevel1 
        Debug.Log("Compétence du Joueur Niv1 débloquer");
        uibuttonCompétence[0].interactable = false;
        uibuttonCompétence[0].enabled = false;
        //bonusLevel[0] = true;
    } 
    public void BonusLevel2()
    {//Active le BonusLevel2
        Debug.Log("Compétence du Joueur Niv 2 débloquer");
        
        uibuttonCompétence[1].interactable = false;
        uibuttonCompétence[1].enabled = false;
      //  bonusLevel[1] = true;
    } 
    public void BonusLevel3()
    {//Active le BonusLevel3
        Debug.Log("Compétence du Joueur Niv3 débloquer");
        uibuttonCompétence[2].interactable = false;
        uibuttonCompétence[2].enabled = false;
       // bonusLevel[2] = true;
    }
    public void BonusLevel4()
    {//Active le BonusLevel4
        Debug.Log("Compétence du Joueur Niv4 débloquer");
        uibuttonCompétence[3].interactable = false;
        uibuttonCompétence[3].enabled = false;
       // bonusLevel[3] = true;
    }
}
