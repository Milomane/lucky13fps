﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public TimeScaler time;
    public GameObject gameOverPanel;
    public float maxHp;
    public float actualHp;
    public GameObject healthbar;
    
    // Start is called before the first frame update
    void Start()
    {
        actualHp = maxHp;
        healthbar.transform.localScale = new Vector3(actualHp / maxHp, 1, 1);
    }

    private void Update()
    {
        if (actualHp <= 0)
        {
            gameOverPanel.SetActive(true);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            time.timeScale = 0f;
           // Destroy(gameObject);
        }
        else if (actualHp > maxHp)
        {
            actualHp = maxHp;
        }
        healthbar.transform.localScale = new Vector3(actualHp / maxHp, 1, 1);
    }

    //fonction qui enlève des hp au joueur en fonction des dégats reçus, elle lance aussi une petite animation
    //de perte de sang et si les HP tombent à 0 alors elle lance l'animation de mort
    public void TakeDamage(int damage)
    {
        actualHp -= damage;
        Debug.Log(actualHp / maxHp);
        gameObject.GetComponent<ParticleSystem>().Play();
    }
}
