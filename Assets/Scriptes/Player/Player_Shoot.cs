﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Shoot : MonoBehaviour
{
   
    public Transform bulletSpawnPosition;
    public GameObject playerBullet;
    

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            // Lance la fonction Shoot 
            Shoot();
        }
    }

 

    void Shoot()
    {
        Instantiate(playerBullet, bulletSpawnPosition.position, Quaternion.identity);
    }
}
