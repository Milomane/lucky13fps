﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed;

    public float jumpForce;
    private bool isGrounded;
    
    public float dashForce;
    public float dashCD;
    
    private float timerDash;
    private float timerBeforeDash;

    public Rigidbody rb;

    private float lastCamPos;
    private string faceInputAxis = "Vertical";
    private string sideInputAxis = "Horizontal";

    public bool isCrouched;

    public Transform aimPoint;

    
    //assignation des variables
    void Start()
    {
        
        isCrouched = true;
        timerDash = 10f;
        gameObject.GetComponent<TrailRenderer>().enabled = false;
        isGrounded = false;
    }

 
    void Update()
    {
        
        //récupération du temps et des axes
        timerDash += Time.deltaTime;
        float faceAxis = Input.GetAxis(faceInputAxis);
        float sideAxis = Input.GetAxis(sideInputAxis);
       
        transform.LookAt(new Vector3(aimPoint.position.x, transform.position.y, aimPoint.position.z));

        //quand le joystick gauche n'est pas au centre alors le player s'oriente comme la caméra et lance la méthode Move
        if (Input.GetAxis("Horizontal") != 0f || Input.GetAxis("Vertical") != 0f)
        {
            Move(faceAxis, sideAxis);
        }
        
        //si le player appuie sur carré et qu'il est au sol alors il saute
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            Jump();
        }

        //si le player appuie sur le boutton Triangle aolrs il effectue un dash vers l'avant
        if (Input.GetButtonDown("Dash"))
        {
            if (timerDash > dashCD)
            {
                StopCoroutine(dash(0, 0));
                StartCoroutine(dash(1f, 0.1f));
            }
        }
        //si on appuie sur le bouton rond et que le joueur n'est pas accroupi il s'accroupi
        if (Input.GetButtonDown("Crouch") && !isCrouched)
        {
            isCrouched = true;
            gameObject.transform.localScale = new Vector3(1f,0.7f,1f);
        }
        //si on appuie sur le bouton rond et que le joueur est accroupi il se relève
        else if (Input.GetButtonDown("Crouch") && isCrouched)
        {
            isCrouched = false;
            gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
        }
    }
    //coroutine dash activant un traileffect, ajoutant de la force vers l'avant pour créer le déplacement et attendant un certain temps avant de se désactiver
    IEnumerator dash(float time, float time2)
    {
        yield return new WaitForSeconds(time2);
        gameObject.GetComponent<TrailRenderer>().enabled = true;
        rb.AddForce(transform.forward * dashForce + new Vector3(0,20,0), ForceMode.Impulse);
        timerDash = 0f;
        yield return new WaitForSeconds(time);
        gameObject.GetComponent<TrailRenderer>().enabled = false;
    }

    //ajout de force verticale pour faire sauter le player
    void Jump()
    {
        rb.AddForce(transform.up * jumpForce * 2, ForceMode.Impulse);
    }
    
    //ajout de force sur les axes X et Z pour faire déplacer le joueur
    void Move(float faceInput, float sideInput)
    {
        rb.AddForce(((transform.forward * faceInput) + (transform.right * sideInput)) * moveSpeed, ForceMode.Force);
    }

    
    //quand le joueur rentre en collision avec le sol, le booléen isGrounded devient true
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("ground"))
            isGrounded = true;
    }

    //quand le joueur quitte une collision avec le sol, le booléen isGrounded devient false
    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("ground"))
            isGrounded = false;
    }
}
