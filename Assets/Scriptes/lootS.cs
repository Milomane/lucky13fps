﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class lootS : MonoBehaviour
{
    public GameObject scrap;
    private int _zero;
    private int _randomInt;

    private float _randomPlaceX;

    private float _randomPlaceY;

    public int numberScrapMin;

    public int numberScrapMax;
    // Start is called before the first frame update
    void Start()
    {
        _randomInt = Random.Range(numberScrapMin, numberScrapMax);
    }

    // Update is called once per frame
    void Update()
    {
      
    }

    private void OnDestroy()
    {
        while (_zero < _randomInt)
        {
            _randomPlaceX = Random.Range(transform.position.x + 1, transform.position.x - 1);
            _randomPlaceY = Random.Range(transform.position.y + 1, transform.position.y - 1);
            Instantiate(scrap, new Vector3(_randomPlaceX,_randomPlaceY, transform.position.z), transform.rotation);
            _zero++;
        }
    }
}
