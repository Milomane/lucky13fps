﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBullet : MonoBehaviour
{
	private GameObject uiImagecrosshair;
	public int damage = 1;
    
	public float speed = 500f;
	public float timeBeforeDestroy = 5f;

	public Vector3 inaccuracyVector;

	public float raycastDistance = 0.05f;
	public LayerMask maskToHit;
	public void Start()
	{
		uiImagecrosshair = GameObject.FindGameObjectWithTag("crosshair");
		Camera camera = Camera.main;
		
		RaycastHit hit;
		if (Physics.Raycast(camera.transform.position,  uiImagecrosshair.transform.position - camera.transform.position, out hit, maskToHit))
		{
			Debug.Log("Solide");
			Debug.Log(hit.collider.name);
			transform.LookAt(hit.point);
		}
		else
		{
			Debug.Log("Aerial");
			transform.LookAt(uiImagecrosshair.transform.position);
		}
		
		GetComponent<Rigidbody>().AddForce((transform.forward) * speed, ForceMode.Impulse);
		Destroy(gameObject, timeBeforeDestroy);
	}

	public void Update()
	{
		RaycastHit[] hit;
		hit = Physics.SphereCastAll(transform.position, raycastDistance, Vector3.forward);
		foreach (RaycastHit h in hit)
		{
			if (h.collider.GetComponent<EnemyHealth>())
			{
				h.collider.GetComponent<EnemyHealth>().TakeDamage(damage);
				Destroy(gameObject);
			}

			if (h.collider.GetComponent<PartBossHealth>())
			{
				h.collider.GetComponent<PartBossHealth>().TakeDamage(damage);
				Destroy(gameObject);
			}
		}
	}

	public void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<EnemyHealth>())
		{
			Debug.Log("Enemy touché");
			
			other.GetComponent<EnemyHealth>().TakeDamage(damage);
			Destroy(gameObject);
		}

		if (other.GetComponent<PartBossHealth>())
		{
			other.GetComponent<PartBossHealth>().TakeDamage(damage);
			Destroy(gameObject);
		}
	}

	public void OnDrawGizmos()
	{
		Gizmos.DrawWireSphere(transform.position, raycastDistance);
	}
}