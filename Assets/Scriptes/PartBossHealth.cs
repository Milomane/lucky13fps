﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartBossHealth : MonoBehaviour
{
    public float maxHealth;
    public float health;
    public float realHealth;
    private Color baseColor;
    public Color dronePhaseColor;
    public Color blinkColor;
    public Color grayPhaseColor;
    public float blinkTime;

    public bool dronePhase;
    public bool grayPhase;
    public bool destroy;

    public GameObject animationDestroyObject;

    private Renderer renderer;

    void Start()
    {
        health = maxHealth;

        if (!GetComponent<BossEye>())
        {
            baseColor = GetComponent<Renderer>().material.color;
            renderer = GetComponent<Renderer>();
        }
    }

    private void Update()
    {
        if (!grayPhase)
        {
            if (health <= 0)
            {
                renderer.material.color = dronePhaseColor;
            }
        }
    }

    public void TakeDamage(int damage)
    {
        if (!grayPhase)
        {
            if (health > 0)
            {
                health -= damage;
            }
            else
            {
                if (destroy)
                {
                    realHealth -= damage;

                    if (realHealth <= 0)
                    {
                        StartCoroutine(Disable());
                    }
                }
            }

            StartCoroutine(Blink());
            Blink();
            Debug.Log("Blink++");
        }
    }

    public IEnumerator Blink()
    {
        renderer.material.color = blinkColor;
        yield return new WaitForSeconds(blinkTime);
        renderer.material.color = baseColor;
    }

    public void DroneAttack()
    {
        if (dronePhase)
        {
            destroy = true;
        }
    }

    public void GrayState()
    {
        if (!destroy)
        {
            Debug.Log("GrayNow");
            renderer.material.color = grayPhaseColor;
            health = Mathf.RoundToInt(health + (maxHealth - health) / 2);
            grayPhase = true;
        }
    }

    public void UndoGrayState()
    {
        if (!destroy)
        {
            renderer.material.color = baseColor;
            grayPhase = false;
        }
    }

    public IEnumerator Disable()
    {
        animationDestroyObject.SetActive(true);
        yield return new WaitForSeconds(3f);
        gameObject.SetActive(false);
    }
}
