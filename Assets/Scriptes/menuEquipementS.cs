﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class menuEquipementS : MonoBehaviour
{
   public  Button[] button = new Button[9];
   public bool[] isButtonSelected = new bool[9];
   public Sprite[] baseImage = new Sprite[9];
   public int selection;

   public GameObject canvas;

   public bool isCanvasActive;

   public float cooldown;

   public float cooldownValue;

   public Sprite choiceImage;

   public Sprite selectedImage;

   public int foreachButton;
   // Start is called before the first frame update
    void Start()
    {
        cooldown = cooldownValue;
        canvas.SetActive(isCanvasActive);
        isButtonSelected[0] = true;
        isButtonSelected[3] = true;
        isButtonSelected[6] = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (cooldown >= 0)
        {
            cooldown -= Time.deltaTime;
        }

         if (selection > 8) 
         {
             selection = 8;
         }
       
         if (selection < 0)
         {
             selection = 0;
         }
               
        if (Input.GetButtonDown("Menu"))
        {
            isCanvasActive = !isCanvasActive;
            canvas.SetActive(isCanvasActive);
        }

        if (isCanvasActive)
        {
            if (Input.GetAxis("Vertical") > 0 && selection <= 2)
            {
                if (cooldown <= 0)
                {
                    cooldown = cooldownValue;
                    selection += 6;
                }
            }
            if (Input.GetAxis("Vertical") < 0 && selection >= 6)
            {
                if (cooldown <= 0)
                {
                    cooldown = cooldownValue;
                    selection -= 6;
                }
            }

            else if (Input.GetAxis("Horizontal") > 0 && cooldown <= 0)
            {
                selection++;
                cooldown = cooldownValue;
            }
            else if (Input.GetAxis("Horizontal") < 0 && cooldown <= 0) 
            {
                selection--;
                cooldown = cooldownValue;
            }
            else if (Input.GetAxis("Vertical") < 0 && cooldown <= 0)
            {
                selection += 3;
                cooldown = cooldownValue;
            }
            else  if (Input.GetAxis("Vertical") > 0 && cooldown <= 0)
            {
                selection -= 3;
                cooldown = cooldownValue;
            }
            SelectionButton(selection);
            SelectedButton(selection);
        }

      
    }

    public void SelectionButton(int buttonNumber)
    {
        button[buttonNumber].image.sprite = choiceImage;
        
        foreach (var buttonObject in button)
            {
                if (foreachButton != buttonNumber)
                {
                    button[foreachButton].image.sprite = baseImage[foreachButton];
                    foreachButton = selection;
                }
            }
    }

    public void SelectedButton(int buttonNumber)
    {
        if (Input.GetButtonDown("Valide"))
        {
            if (isButtonSelected[1] || isButtonSelected[2])
            {
                if (selection == 0)
                {
                    isButtonSelected[0] = true;
                    isButtonSelected[1] = false;
                    isButtonSelected[2] = false;
                }
            } 
            if (isButtonSelected[0] || isButtonSelected[2])
            {
                if (selection == 1)
                {
                    isButtonSelected[1] = true;
                    isButtonSelected[0] = false;
                    isButtonSelected[2] = false;
                }
            } 
            if (isButtonSelected[0] || isButtonSelected[1])
            {
                if (selection == 2)
                {
                    isButtonSelected[2] = true;
                    isButtonSelected[0] = false;
                    isButtonSelected[1] = false;
                }
            } 
            if (isButtonSelected[4] || isButtonSelected[5])
            {
                if (selection == 3)
                {
                    isButtonSelected[3] = true;
                    isButtonSelected[4] = false;
                    isButtonSelected[5] = false;
                }
            } 
            if (isButtonSelected[3] || isButtonSelected[5])
            {
                if (selection == 4)
                {
                    isButtonSelected[4] = true;
                    isButtonSelected[3] = false;
                    isButtonSelected[5] = false;
                }
            } 
            if (isButtonSelected[3] || isButtonSelected[4])
            {
                if (selection == 5)
                {
                    isButtonSelected[5] = true;
                    isButtonSelected[3] = false;
                    isButtonSelected[4] = false;
                }
            } 
            if (isButtonSelected[7] || isButtonSelected[8])
            {
                if (selection == 6)
                {
                    isButtonSelected[6] = true;
                    isButtonSelected[7] = false;
                    isButtonSelected[8] = false;
                }
            } 
            if (isButtonSelected[6] || isButtonSelected[8])
            {
                if (selection == 7)
                {
                    isButtonSelected[7] = true;
                    isButtonSelected[6] = false;
                    isButtonSelected[8] = false;
                }
            } 
            if (isButtonSelected[6] || isButtonSelected[7])
            {
                if (selection == 8)
                {
                    isButtonSelected[8] = true;
                    isButtonSelected[6] = false;
                    isButtonSelected[7] = false;
                }
            } 
       
        }
    }
}
