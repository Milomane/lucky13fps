﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossLaser : MonoBehaviour
{
    public bool active;
    public float cooldown;
    public float maxDistance;
    
    public float duration;
    private float timer;

    public LineRenderer lr;
    public GameObject particle;


    void Start()
    {
        lr.SetPosition(0, transform.position);
    }

    void Update()
    {
        timer -= Time.deltaTime;
        
        if (timer > 0)
        {
            transform.LookAt(FindObjectOfType<PlayerLastPos>().lastPos);
            
            RaycastHit hit;
            if (Physics.Raycast(transform.position, (FindObjectOfType<PlayerLastPos>().lastPos - transform.position).normalized, out hit, maxDistance))
            {
                if (hit.collider.GetComponent<PlayerHealth>())
                {
                    hit.collider.GetComponent<PlayerHealth>().TakeDamage(1);
                }

                if (hit.collider)
                {
                    lr.SetPosition(1, hit.point + transform.forward * 0.5f);
                    particle.SetActive(true);
                    particle.transform.position = hit.point;
                }
            }
            else
            {
                lr.SetPosition(1, transform.forward * maxDistance);
                particle.SetActive(false);
            }
        }
        else
        {
            timer = 0;
            lr.SetPosition(1, transform.position);
            particle.SetActive(false);
        }
    }
    
    public void Shoot()
    {
        timer = duration;
    }
}
