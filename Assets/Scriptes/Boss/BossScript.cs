﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using Random = UnityEngine.Random;

public class BossScript : MonoBehaviour
{
    public BossCanon canonLeft;
    public BossCanon canonRight;
    public BossEye eyes;
    public BossLaser laser;
    public BossSpawner spawner;

    private PartBossHealth canonLeftHealth, canonRightHealth, eyesHealth, laserHealth, spawnerHealth;

    private PartBossHealth currentPartTargeted;

    private float timer;

    private int phase = 1;

    public float bossDetectionDistance;

    private DroneController _droneController;

    void Start()
    {
        canonLeftHealth = canonLeft.gameObject.GetComponent<PartBossHealth>();
        canonRightHealth = canonLeft.gameObject.GetComponent<PartBossHealth>();
        eyesHealth = canonLeft.gameObject.GetComponent<PartBossHealth>();
        laserHealth = canonLeft.gameObject.GetComponent<PartBossHealth>();
        spawnerHealth = canonLeft.gameObject.GetComponent<PartBossHealth>();

        _droneController = FindObjectOfType<DroneController>();
    }

    void Update()
    {
        if (FindObjectOfType<PlayerHealth>())
        {
            if (Vector3.Distance(FindObjectOfType<PlayerHealth>().transform.position, transform.position) <= bossDetectionDistance)
            {
                switch (phase)
                {
                    case 1:
                       // Debug.Log("Debut");
                        // Check health of all parts
                        if (canonLeftHealth.health <= 0)
                        {
                            Debug.Log("Canon left damaged");
                            if (canonLeft.active)
                            {
                                Phase2Switch(canonLeftHealth);
                            }
                        }
                        if (canonRightHealth.health <= 0)
                        {
                            Debug.Log("Canon right damaged");
                            if (canonRight.active)
                            {
                                Debug.Log("CanonRighttargeted");
                                Phase2Switch(canonRightHealth);
                            }
                        }
                        if (eyesHealth.health <= 0)
                        {
                            Debug.Log("Eye damaged");
                            if (eyes.active)
                            {
                                Phase2Switch(eyesHealth);
                            }
                        }
                        if (laserHealth.health <= 0)
                        {
                            Debug.Log("Laser damaged");
                            if (laser.active)
                            {
                                Phase2Switch(laserHealth);
                            }
                        }
                        if (spawnerHealth.health <= 0)
                        {
                            Debug.Log("Spawner damaged");
                            if (spawner.active)
                            {
                                Phase2Switch(spawnerHealth);
                            }
                        }

                        if (timer > 0)
                        {
                            timer -= Time.deltaTime;
                        }
                        else
                        {
                            int chooseWeapon = Random.Range(0,5);
                            
                            bool weaponSelected = false;
                            int count = 0;
                                
                            while (!weaponSelected)
                            {
                                switch (chooseWeapon)
                                {
                                    case 0:
                                        if (canonLeft.active)
                                        {
                                            CanonShoot(canonLeft);
                                            weaponSelected = true;
                                        }
                                        break;
                                
                                    case 1:
                                        if (canonRight.active)
                                        {
                                            CanonShoot(canonRight);
                                            weaponSelected = true;
                                        }
                                        break;
                                
                                    case 2:
                                        if (eyes.active)
                                        {
                                            EyesShoot();
                                            weaponSelected = true;
                                        }
                                        break;
                                
                                    case 3:
                                        if (laser.active)
                                        {
                                            LaserShoot();
                                            weaponSelected = true;
                                        }
                                        break;
                                
                                    case 4:
                                        if (spawner.active)
                                        {
                                            SpawnerShoot();
                                            weaponSelected = true;
                                        }
                                        break;
                                }
                                
                                count++;
                                if (count >= 5) return; // Boss is DEAD
                                
                                chooseWeapon++;
                                if (chooseWeapon > 4) chooseWeapon = 0;
                            }
                        }
                        
                        break;
                    case 2:
                        Debug.Log("Phase2");
                        if (currentPartTargeted.destroy)
                        {
                            _droneController.isActive = false;
                            phase = 3;
                        }
                        break;
                    case 3:
                        if (currentPartTargeted.realHealth <= 0)
                        {
                            UnderGrayState();
                        }
                        break;
                }
            }
        }

        if (eyes.active == false && canonLeft.active == false && canonRight.active == false &&
            spawner.active == false && laser.active == false)
        {
            // Boss death
            Destroy(gameObject);
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            spawnerHealth.GrayState();
        }
    }

    public void CanonShoot(BossCanon canon)
    {
        canon.Shoot();
        timer = canon.cooldown;
    }

    public void EyesShoot()
    {
        eyes.Shoot();
        timer = eyes.cooldown;
    }

    public void LaserShoot()
    {
        laser.Shoot();
        timer = laser.cooldown;
    }

    public void SpawnerShoot()
    {
        spawner.Shoot();
        timer = spawner.cooldown;
    }


    public void Phase2Switch(PartBossHealth partHealthScript)
    {
        Debug.Log("phase2switch");
        // Init phase 2 for the object targeted
        phase = 2;
        partHealthScript.dronePhase = true;
        currentPartTargeted = partHealthScript;
        _droneController.isActive = true;
        
        
        // Gray all other part
        if (canonLeftHealth != partHealthScript)
        {
            Debug.Log("CanonLeftGray");
            canonLeftHealth.GrayState();
        }

        if (canonRightHealth != partHealthScript)
        {
            canonRightHealth.GrayState();
        }

        if (eyesHealth != partHealthScript)
        {
            eyesHealth.GrayState();
        }

        if (laserHealth != partHealthScript)
        {
            laserHealth.GrayState();
        }

        if (spawnerHealth != partHealthScript)
        {
            spawnerHealth.GrayState();
        }
    }

    public void UnderGrayState()
    {
        canonLeftHealth.UndoGrayState();
        canonRightHealth.UndoGrayState();
        eyesHealth.UndoGrayState();
        laserHealth.UndoGrayState();
        spawnerHealth.UndoGrayState();

        if (canonLeftHealth.realHealth <= 0)
        {
            canonLeft.active = false;
        }
        if (canonRightHealth.realHealth <= 0)
        {
            canonRight.active = false;
        }
        if (eyesHealth.realHealth <= 0)
        {
            eyes.active = false;
        }
        if (laserHealth.realHealth <= 0)
        {
            laser.active = false;
        }
        if (spawnerHealth.realHealth <= 0)
        {
            spawner.active = false;
        }
        
        phase = 1;
    }

    public void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, bossDetectionDistance);
    }
}
