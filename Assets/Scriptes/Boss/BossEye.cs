﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class BossEye : MonoBehaviour
{
    public bool active;
    public float cooldown;

    public BoxCollider objCollider;
    public Camera cam;
    public Plane[] planes;
    
    
    public bool visible;

    private GameObject fade;
    public float fadeTime;


    public void Start()
    {
        fade = GameObject.FindGameObjectWithTag("Fade");
        cam = Camera.main;
        objCollider = GetComponent<BoxCollider>();
    }

    public void Update()
    {
        planes = GeometryUtility.CalculateFrustumPlanes(cam);
        
        if (GeometryUtility.TestPlanesAABB(planes, objCollider.bounds)) 
        {
            visible = true;
        }
        else
        {
            visible = false;
        }
    }

    public void Shoot()
    {
        if (visible)
        {
            StartCoroutine(ResetLevel());
        }
    }

    IEnumerator ResetLevel()
    {
        fade.GetComponent<Image>().material.DOFade(1, fadeTime);
        
        yield return new WaitForSeconds(fadeTime);
        
        Application.LoadLevel(Application.loadedLevel);
    }
}
