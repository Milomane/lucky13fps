﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossCanon : MonoBehaviour
{
    public bool active;
    public float cooldown;
    public float inaccuracy = 1;

    public GameObject prefabBullet;
    public Transform shootPos;
    
    
    public void Shoot()
    {
        Vector3 inaccuracyVector = new Vector3(Random.Range(inaccuracy, -inaccuracy), Random.Range(inaccuracy, -inaccuracy), Random.Range(inaccuracy, -inaccuracy));
        
        GameObject bullet = Instantiate(prefabBullet, shootPos.transform.position, Quaternion.identity);
        bullet.GetComponent<EnnemyBullet>().target = FindObjectOfType<PlayerHealth>().transform;
        bullet.GetComponent<EnnemyBullet>().inaccuracyVector = inaccuracyVector;
    }
}
