﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawner : MonoBehaviour
{
    public bool active;
    public float cooldown;

    public GameObject prefabMob;
    public Transform spawnPosition;
    public float randomMaxForce;

    public int minEnemy;
    public int maxEnemy;

    public float TimeBetweenEnemies;


    public void Shoot()
    {
        StartCoroutine(Spawning(Random.Range(minEnemy, maxEnemy)));
    }

    IEnumerator Spawning(float numberOfMobs)
    {
        for (int i = 0; i < numberOfMobs; i++)
        {
            GameObject enemy = Instantiate(prefabMob, spawnPosition.position, Quaternion.identity);
            enemy.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-randomMaxForce, randomMaxForce), Random.Range(-randomMaxForce, randomMaxForce), Random.Range(-randomMaxForce, randomMaxForce)));
            enemy.GetComponent<FolowPath>().target = FindObjectOfType<PlayerHealth>().transform;
            
            yield return new WaitForSeconds(TimeBetweenEnemies);
        }
    }
}
