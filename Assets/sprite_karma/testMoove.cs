﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testMoove : MonoBehaviour
{
    public float Speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PlayerMovement();
    }

    void PlayerMovement()
    {
        float hor = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");
        Vector3 playermoovement = new Vector3(hor, 0f, vert) * Speed * Time.deltaTime;
        transform.Translate(playermoovement, Space.Self);
    }
}
